#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  7 11:04:00 2019

@author: sohel
    
"""

from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import Normalizer
from sklearn.preprocessing import Binarizer
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import train_test_split

class Scaller(object):
    
    def __init__(self):
        pass
    
    def minmaxscaler(self,dataset_M):
        scaler_m=MinMaxScaler(feature_range=(0,1))
        scaledData_M=scaler_m.fit_transform(dataset_M)
        return(scaledData_M)
    
    def binarizer(self,dataset_B):
        scaler_b=Binarizer(threshold=0.5)
        scaledData_B=scaler_b.fit_transform(dataset_B)
        return(scaledData_B)
        
    def normalizer(self,dataset_N):
        scaler_n=Normalizer()
        scaledData_N=scaler_n.fit_transform(dataset_N)
        return(dataset_N)
        
    def standardsc(self,dataset_S):
        scaler_s=StandardScaler()
        scaledData_S=scaler_s.fit_transform(dataset_S)
        return(dataset_S)  


    def __del__(self):
        pass
    
class splitter(object):
    
    def __init__(self):
        pass
    
    def decomposition(self,x,y,randomState=0,testSize=1/4):
        x_train,x_test,y_train,y_test=train_test_split(x,y,test_size=testSize,random_state=randomState)
        return(x_train,x_test,y_train,y_test)
        
    