#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  7 13:48:45 2019

@author: sohel
"""

from sklearn.linear_model import LinearRegression

class Lregression(object):
    
    def __init__(self):
        pass
    
    def regression(self,dataX,dataY):
        regressor=LinearRegression()
        regressor.fit(dataX,dataY)
        return(regressor)
        
    def __del__(self):
        pass